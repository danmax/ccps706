#!/bin/bash
#Author: Daniel Siegel
#For CCPS706

if [ $# = 1 ]

then

    while :
    do
        lastline=`tail -1 $1`
        curl --data "data=$lastline" ryersonserver.com/ajax/post_data.php
        cp $1 $1.tmp
        sed '$ d' $1.tmp > $1
        rm -f $1.tmp
        sleep 1
    done

else 

echo "Usage: $0 /Path/To/Log/logname.txt"
exit 0

fi