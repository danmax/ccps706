# README #

This README describes the process of deploying this project.

### What is this repository for? ###

This project is for CCPS706, to monitor the servers.

* Version
1.0.0

### How do I get set up? ###

* Summary of set up

* Configuration
get repo:
git clone https://danmax@bitbucket.org/danmax/ccps706.git

If the repository is private and a password is required, please contact Daniel Siegel (see contact information at the bottom)

touch includes/pslconfig.php

Insert as follows:

<?php
// Configure the following with your DB info

define("HOST", "localhost"); 			// The host you want to connect to.
define("USER", "root"); 			// The database username.
define("PASSWORD", ""); 	// The database password.
define("DATABASE", "ccps706");             // The database name.

define("HOME_URL", "localhost/ccps706");             // The database name.

define("CAN_REGISTER", "any");
define("DEFAULT_ROLE", "member");

// If you are using an HTTPS connection, change this to TRUE
define("SECURE", FALSE);
?>

Add the SQL from the root to your mysql
Make yourself a user on the site
you're done

### Who do I talk to? ###

* Repo owner or admin
Daniel Max Siegel
d2siegel
5004442241
daniel.siegel@ryerson.ca

Please contact in case of difficulties.