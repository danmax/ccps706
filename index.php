<?php
    include_once 'includes/db_connect.php';
    include_once 'includes/functions.php';
    sec_session_start();

    $out = '';

    if (login_check($mysqli) == true) {
        redirect('./view.php');
    }

    $out .= get_header("View Server Data - Login");

    if (isset($_GET['error'])) {
        $out .= '<p class="error">Error Logging In!</p>';
    }

    $out .= '
        <div class="login-container">
            <div class="login-inner">
                <form action="includes/process_login.php" method="post" name="login_form"> 	
                    <h2 class="chart-desc">Email</h2><br/>		
                    <input class="input-text" type="text" name="email" placeholder="email"/>
                    <br/>
                    <br/>
                    <h2 class="chart-desc">Password</h2><br/>
                    <input class="input-text" 
                                    type="password" 
                                     name="password" 
                                     id="password"
                                     placeholder="password"/>
                    <br/>
                    <br/><br/>
                    <input class="login-button"
                            type="submit" 
                           value="Login" 
                           onclick="formhash(this.form, this.form.password);" /> 
                </form>
            </div>
        </div>
    ';
   
    $out .= get_footer();

    echo $out;