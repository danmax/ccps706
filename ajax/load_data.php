<?php
    $out = '';

    require_once("../includes/psl-config.php");
    require_once("../includes/db_connect.php");
    require_once("../includes/functions.php");

    $q = "SELECT temperature1,temperature2,airspeed,timestamp FROM `data` order by timestamp desc limit 200";
    $r = $mysqli->query($q) or die($mysqli->error.__LINE__);

    $data=array();
    while($row = $r->fetch_assoc()) {
        $data[] = $row;
    }

    $q2 = "SELECT id,water_level FROM `data` order by timestamp desc limit 1";
    $r2 = $mysqli->query($q2) or die($mysqli->error.__LINE__);

    $data2=array();
    while($row2 = $r2->fetch_assoc()) {
        $w_data = $row2;
    }
    $water_level = $w_data['water_level'];

    $out .= "  
        <h1 class='chart-title'>
            Temperature
        </h1>
        <h2 class='chart-desc'>
            In degrees celcius
        </h2>
        <div id='temp_chart_div'></div>

        <h1 class='chart-title'>
            Airspeed
        </h1>
        <h2 class='chart-desc'>
            In kPa
        </h2>
        <div id='airspeed_chart_div'></div>
        <br/>
    ";

    $out .= "
        <div style='margin-top:20px;'>
            Water Level: <span class='water-level-$water_level'>$water_level</span>
        </div>
            ";

    

    $out .= get_nestable_js($data);
    echo $out;

    function get_nestable_js($data){
        $out = "";

        $temp_html = '';
        $temp_speed = '';
        $data_printed=1;
        $data = array_reverse($data);
        foreach($data as $d){
            $temp_html .= '["';
            // $temp_html .= $d['timestamp'];
            $temp_html .= date('H:i:s', $d['timestamp']);
            $temp_html .= '",';
            $temp_html .= $d['temperature1'];
            $temp_html .= ',';
            $temp_html .= $d['temperature2'];
            $temp_html .= ']';

            $temp_speed .= '["';
            $temp_speed .= date('H:i:s', $d['timestamp']);
            $temp_speed .= '",';
            $temp_speed .= $d['airspeed'];
            $temp_speed .= ']';

            if(!(count($data)==$data_printed)){
                $temp_html .= ',';
                $temp_speed .= ',';
            }
            ++$data_printed;
        }

        $out .= "
            <script>
                google.charts.setOnLoadCallback(drawChart);
            
                function drawChart() {
                    var data = new google.visualization.DataTable();
                    data.addColumn('string', 'Time (hrs:mins:secs)');
                    data.addColumn('number', 'Temperature1');
                    data.addColumn('number', 'Temperature2');

                    data.addRows([
                        $temp_html
                    ]);

                    var options = {
                        chart: {
                            title: '',
                            subtitle: ''
                        },
                        width: 800,
                        height: 200
                    };

                    var chart = new google.charts.Line(document.getElementById('temp_chart_div'));
                    chart.draw(data, options);

                    var data2 = new google.visualization.DataTable();
                    data2.addColumn('string', 'Time (hrs:mins:secs)');
                    data2.addColumn('number', 'Airspeed');

                    data2.addRows([
                        $temp_speed
                    ]);

                    var options2 = {
                        chart2: {
                            title: '',
                            subtitle: ''
                        },
                        width: 800,
                        height: 200
                    };

                    var chart2 = new google.charts.Line(document.getElementById('airspeed_chart_div'));
                    chart2.draw(data2, options2);
                }
            </script>
        ";

        return $out;
    }