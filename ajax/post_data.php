<?php
    require_once("../includes/psl-config.php");
    require_once("../includes/db_connect.php");
    require_once("../includes/functions.php");

    $data = $_POST['data'];

    $sql = "INSERT INTO rawdata (data)
                        VALUES ('$data')";

    $mysqli->query($sql);

    // $data = "80808032335B419C99D9400100C03F00";

    $data = str_replace("\n", "", $data);
    $data = str_replace("\r", "", $data);
    $count = strlen($data);
    if($count == 32){
        if(substr($data, 0, 6) == '808080'){
            $temperature1 = hexTo32Float(substr($data, 6, 8));
            $temperature2 = hexTo32Float(substr($data, 14, 8));
            $airspeed = hexTo32Float(substr($data, 22, 8));
            $water_level = hexdec(substr($data, 30, 2));

            $timestamp = time();
            $thirty_seconds_ago = $timestamp - 30;

            $sql = "INSERT INTO data (timestamp,temperature1,temperature2,airspeed,water_level)
                                VALUES ('$timestamp','$temperature1','$temperature2','$airspeed','$water_level')";

            $mysqli->query($sql);

            //send emails
            $q = "SELECT * FROM `email_notificats`";
            $r = $mysqli->query($q) or die($mysqli->error.__LINE__);

            $email_data=array();
            while($row = $r->fetch_assoc()) {
                $email_data[] = $row;
            }

            foreach($email_data as $email){
                //get data from condition
                $curr_id = $email['id'];
                $curr_email = $email['email'];
                $curr_field = $email['field'];
                $curr_cond = $email['cond'];
                $curr_val = $email['val'];
                $last_time_emailed = $email['last_time_emailed'];

                //thirty seconds ago = 90
                //last_time_emailed = 100
                //now = 120

                if($thirty_seconds_ago >= $last_time_emailed){
                    //has condition for email been met?
                    $val_to_consider = 0;
                    switch($curr_field){
                        case "temperature1":
                            $val_to_consider = $temperature1;
                            break;
                        case "temperature2":
                            $val_to_consider = $temperature2;
                            break;
                        case "airspeed":
                            $val_to_consider = $airspeed;
                            break;
                        case "water_level":
                            $val_to_consider = $water_level;
                            break;
                        default:
                            break;
                    }

                    $should_send_email = false;
                    switch($curr_cond){
                        case "lt":
                            if ($curr_val > $val_to_consider){
                                $should_send_email = true;
                                $equals_text = "<";
                            }
                            break;
                        case "lte":
                            if ($curr_val >= $val_to_consider){
                                $should_send_email = true;
                                $equals_text = "<=";
                            }
                            break;
                        case "e":
                            if ($curr_val == $val_to_consider){
                                $should_send_email = true;
                                $equals_text = "==";
                            }
                            break;
                        case "gte":
                            if ($curr_val <= $val_to_consider){
                                $should_send_email = true;
                                $equals_text = ">=";
                            }
                            break;
                        case "gt":
                            if ($curr_val < $val_to_consider){
                                $should_send_email = true;
                                $equals_text = ">";
                            }
                            break;
                        default:
                            break;
                    }

                    // $should_send_email=true;

                    if($should_send_email){
                        require_once('../includes/PHPMailer/PHPMailerAutoload.php');
                        $mail = new PHPMailer;
                        $mail->setFrom('danielmaxsiegel@gmail.com', 'Ryerson Server');
                        $mail->addReplyTo('danielmaxsiegel@gmail.com', 'Ryerson Server');
                        $mail->addAddress($curr_email, $curr_email);
                        $mail->Subject = 'Notification from Ryerson Server';

                        $msg_body = '';
                        $msg_body .= "
                            <html>
                                <body>
                                    Hello,
                                    <br/><br/>
                                    You wanted to be notified if $curr_field was $equals_text $curr_val.
                                    <br/><br/>
                                    Right now it is $val_to_consider.
                                </body>
                            </html>
                            ";
                        $mail->msgHTML( $msg_body );

                        $mail->send();

                        $q = "UPDATE email_notificats SET last_time_emailed='$timestamp' WHERE id=$curr_id";
                        $mysqli->query($q);
                    }
                }
            }
        }
    }
