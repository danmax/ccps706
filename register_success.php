<?php
    $out = '';
    $out .= get_header("Registration successful");
    $out .= '
        <h1>Registration successful!</h1>
        <p>You can now go back to the <a href="index.php">login page</a> and log in</p>
        ';
    $out .= get_footer();
    p($out);
?>