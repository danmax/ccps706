<?php
    include_once 'includes/db_connect.php';
    include_once 'includes/functions.php';
    sec_session_start();

    $user_id = $_SESSION['user_id'];
    $out = "";

    //check if admin and logged in
    if (!login_check($mysqli)) {
        redirect(HOME_URL);
    }

    $notification = "";

    if(isset($_POST['form']) && $_POST['form'] == 'add-mail'){
        $email = $_POST['email'];
        $field = $_POST['field'];
        $cond = $_POST['cond'];
        $val = $_POST['val'];

        $sql = "INSERT INTO email_notificats (email,field,cond,val)
                            VALUES ('$email','$field','$cond','$val')";
        $mysqli->query($sql);

        $notification .= "Email record created";
    } else if (isset($_POST['form']) && $_POST['form'] == 'delete') {
        $id_to_del = $_POST['id_to_del'];
        $sql = "DELETE FROM email_notificats WHERE id=$id_to_del";
        $mysqli->query($sql);
        $notification .= "Email record deleted";
    } else if (isset($_POST['form']) && $_POST['form'] == 'test_email') {
        require_once('includes/PHPMailer/PHPMailerAutoload.php');
        $mail = new PHPMailer;
        $mail->setFrom('danielmaxsiegel@gmail.com', 'Ryerson Server');
        $mail->addReplyTo('danielmaxsiegel@gmail.com', 'Ryerson Server');
        $mail->addAddress($_POST['email_to_email'], $_POST['email_to_email']);
        $mail->Subject = 'Notification from Ryerson Server';

        $msg_body = '';
        $msg_body .= "
            <html>
                <body>
                    Test email!
                </body>
            </html>
            ";
        $mail->msgHTML( $msg_body );

        $mail->send();
    }

    $out .= get_header("View Server Data");

    $out .= "
        <div class='dock'>
            <div class='container'>
            ";

    if($notification != ""){
        $out .= "
            $notification
        ";
    }

    $out .= "
                <div style='width:90%;background-color:#fff;margin:0 auto;'>
                    <div id='graphs'>Loading...</div>
                </div>

                <h2>Notifications</h2>
            ";

    $q = "SELECT * FROM `email_notificats`";
    $r = $mysqli->query($q) or die($mysqli->error.__LINE__);

    $email_data=array();
    while($row = $r->fetch_assoc()) {
        $email_data[] = $row;
    }

    $out .= "
                <h3>Add new:</h3>
                <form method='post'>
                    <input type='hidden' name='form' value='add-mail'>
                    <table border='1'>
                        <tr>
                            <td>Send email to:</td>
                            <td><input type='email' name='email' placeholder='email address'></td>
                        </tr>
                        <tr>
                            <td>When:</td>
                            <td>
                                <select name='field'>
                                  <option value='temperature1'>Temperature 1</option>
                                  <option value='temperature2'>Temperature 2</option>
                                  <option value='airspeed'>Airspeed</option>
                                  <option value='water_level'>Water Level</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Is:</td>
                            <td>
                                <select name='cond'>
                                  <option value='lt'>Less than</option>
                                  <option value='lte'>Less than or equal to</option>
                                  <option value='e'>Equal to</option>
                                  <option value='gte'>Greater than or equal to</option>
                                  <option value='gt'>Greater than</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Value:</td>
                            <td><input type='number' name='val' placeholder='0'></td>
                        </tr>
                        <tr>
                            <td>Submit:</td>
                            <td><input type='submit' name='submit'></td>
                        </tr>
                    </table>
                </form>

                <h3>Current sendouts:</h3>
                <table border='1'>
                <tr>
                    <td>
                        Email
                    </td>
                    <td>
                        Field
                    </td>
                    <td>
                        Condition
                    </td>
                    <td>
                        Value
                    </td>
                    <td>
                        Delete?
                    </td>
                </tr>
    ";

    foreach($email_data as $e_data){
        $curr_id = $e_data['id'];
        $curr_email = $e_data['email'];
        $curr_field = $e_data['field'];
        $curr_cond = $e_data['cond'];
        $curr_val = $e_data['val'];

        switch($curr_cond){
            case "lt":
                $curr_cond = "less than";
                break;
            case "lte":
                $curr_cond = "less than or equal to";
                break;
            case "e":
                $curr_cond = "equal to";
                break;
            case "gte":
                $curr_cond = "greater than or equal to";
                break;
            case "gt":
                $curr_cond = "greater than";
                break;
        }

        $out .= "
            <tr>
                <td>
                    $curr_email
                </td>
                <td>
                    $curr_field
                </td>
                <td>
                    $curr_cond
                </td>
                <td>
                    $curr_val
                </td>
                <td>
                    <form method='POST'>
                        <input type='hidden' name='form' value='delete'>
                        <input type='hidden' name='id_to_del' value='$curr_id'>
                        <input type='submit' name='submit' value='Delete'>
                    </form>
                </td>
            </tr>
        ";
    }

    $out .= "
                </table>

                <br/><br/><br/><br/>
                <form method='post'>
                    <input type='hidden' name='form' value='test_email'>
                    <input type='email' name='email_to_email'>
                    <input type='submit' name='submit' value='send test email'>
                </form>

            </div>
        </div>
    ";

    $out .= get_footer();

    $out .= get_graphs_js();

    echo $out;


    //send emails
    // $q = "SELECT * FROM `email_notificats`";
    // $r = $mysqli->query($q) or die($mysqli->error.__LINE__);
    // $email_data=array();
    // while($row = $r->fetch_assoc()) {
    //     $email_data[] = $row;
    // }
    // foreach($email_data as $email){
    //     $curr_id = $email['id'];
    //     $curr_email = $email['email'];
    //     $curr_field = $email['field'];
    //     $curr_cond = $email['cond'];
    //     $curr_val = $email['val'];

    //     $val_to_consider = 0;
    //     $should_send_email = false;

    //     $data = "80808032335B419C99D9400100C03F00";
    //     $temperature1 = hexTo32Float(substr($data, 6, 8));
    //     $temperature2 = hexTo32Float(substr($data, 14, 8));
    //     $airspeed = hexTo32Float(substr($data, 22, 8));
    //     $water_level = hexdec(substr($data, 30, 2));

    //     p($temperature1);
    //     p($temperature2);
    //     p($airspeed);
    //     p($water_level);
    //     // $val = 13.444;
    //     // p(14 > $val);

    //     switch($curr_field){
    //         case "temperature1":
    //             $val_to_consider = $temperature1;
    //             break;
    //         case "temperature2":
    //             $val_to_consider = $temperature2;
    //             break;
    //         case "airspeed":
    //             $val_to_consider = $airspeed;
    //             break;
    //         case "water_level":
    //             $val_to_consider = $water_level;
    //             break;
    //         default:
    //             break;
    //     }



    // }

    // p('<br/><br/><br/>');


    function get_graphs_js(){
        $ret = '';

        $ret .= "
            <script>
                google.charts.load('current', {'packages':['line']});

                $(document).ready(function()
                {   
                    load();
                    setInterval(load, 2000);
                    // setInterval(get_test_data, 200);
                });

                function load(){
                    var loadUrl = './ajax/load_data.php';                   
                    $('#graphs').load(loadUrl);
                }

                function get_test_data(){
                    $.ajax({
                        url: './ajax/post_test_data.php',
                        method: 'POST'
                    });
                }
                
            </script>
        ";

        return $ret;
    }